GOF Factory pattern.

Load Factory Project.
Open Test VI
Note: project and test vi only depend on Source Interface.
Examine Source:Factory.vi
Run Test.vi
Note dependencies loaded into memory.
Select a different Source.
Reexamine dependencies in memory.
Note that old source classes that have already been used still stay in memory even after VI is done running
Close Test VI FP and note that there are no longer dependencies in memory.


Dynamic Loading and Unloading of Classes Example

Open Dynamic Unloading Project. project.
Open the dependencies. Note just some stuff in vilib.
Run the Test Dynamic Unload VI.
At the breakpoint, examine what is in memory under dependencies.
Unpause the breakpoint, and reexamine what is in memory.
Change the dropdown to select a different source and repeat.

Note that the test vi does not load any of the class heirarchy into memory. 
When searching for plugins, it does everything by path.
When running the wrapper it specifies it by path.
The wrapper is launched by the Run VI Method.
When the wrapper goes out of memory so do the classes.

Dynamic unloading inspired by Mattias Baudot's presontation at GDevCon #1 on PPLs.
